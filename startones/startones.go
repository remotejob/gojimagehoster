package startones

import (
	"gitlab.com/remotejob/gojimagehoster/domains"
	"gopkg.in/gcfg.v1"

	"log"
	"log/syslog"
)

var config domains.Config

//func Start(golog syslog.Writer) ([]string,map[string]struct{}) {
func Start() (syslog.Writer, domains.Config) {

	golog, err := syslog.New(syslog.LOG_ERR, "golog")

	defer golog.Close()
	if err != nil {
		log.Fatal("error writing syslog!!")
	}

	//	golog.Info("StartOnes")

	err = gcfg.ReadFileInto(&config, "/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/gojimagehoster/config.ini")
	if err != nil {

		golog.Crit("cannot read configuration file config.ini" + err.Error())
		//		return nil,nil
	}

	golog.Info(config.Database.ConStr)

	return *golog, config

}
